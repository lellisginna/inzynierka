//
//  AppDelegate.swift
//  LaCollect
//
//  Created by Lellis Ginna on 18.01.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.statusBarStyle = .lightContent
        IQKeyboardManager.sharedManager().enable = true
        
        Router.default.setupAppNavigation(appNavigation: LCAppNavigation())
        
        Realm.Configuration.defaultConfiguration.deleteRealmIfMigrationNeeded = true
        RealmWebService.logged ? openCategories() : openLogin()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}

extension AppDelegate {
    func openLogin() {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "OutsideNavigationController")
        openViewControllerWithTransition(vc)
    }
    
    func openCategories() {
        let vc = UIStoryboard(name: "Categories", bundle: nil).instantiateViewController(withIdentifier: "InsideNavigationController")
        openViewControllerWithTransition(vc)
    }
    
    func openViewControllerWithTransition(_ vc: UIViewController) {
        let transition = CATransition()
        transition.type = kCATransitionFromTop
        window?.set(rootViewController: vc, withTransition: transition)
    }
}

