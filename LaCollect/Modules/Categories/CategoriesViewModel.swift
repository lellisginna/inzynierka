//
//  CategoriesViewModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 26.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import RxSwift
import RealmSwift
import RxDataSources

protocol CategoryViewPresentable {
    var id: String? { get }
    var name: String? { get }
    var desc: String? { get }
    var coverPhoto: Data? { get }
}

class CategoryItemViewModel: CategoryViewPresentable {
    var id: String? = "0"
    var name: String?
    var desc: String?
    var coverPhoto: Data?
    
    init(id: String?, name: String?,desc: String?, coverPhoto: Data?) {
        self.id = id
        self.name = name
        self.desc = desc
        self.coverPhoto = coverPhoto
    }
}

class CategoriesViewModel: HasRealmDatabase {
    
    var realmDatabase: RealmDatabaseManager
    var categories: Variable<[CategoryViewPresentable]> = Variable([])
    var notificationToken: NotificationToken? = nil
    
    init() {
        //database = RealmDatabaseManager.shared
        realmDatabase = RealmDatabaseManager.shared
        
        let categoriesDB = realmDatabase.realm.objects(CategoryModel.self).sorted(byKeyPath: "createdAt", ascending: false)
        notificationToken = categoriesDB.observe({ [weak self] (changes: RealmCollectionChange) in
            guard let me = self else { return }
            switch changes {
            case .initial:
                categoriesDB.forEach({ (category) in
                    me.categories.value.append(me.mapForPresentable(category))
                })
            case .update(_, let deletions, let insertions, let modifications):
                insertions.forEach({ (index) in
                    let categoryEntity = categoriesDB[index]
                    let categoryViewModel = me.mapForPresentable(categoryEntity)
                    me.categories.value.insert(categoryViewModel, at: 0)
                })
                
                modifications.forEach({ (index) in
                    let categoryEntity = categoriesDB[index]
                    
                    if categoryEntity.isInvalidated {
                        self?.categories.value.remove(at: index)
                    }
                    
                    if let cat = RealmDatabaseManager.shared.realm.object(ofType: CategoryModel.self,
                                                                          forPrimaryKey: categoryEntity.id),
                        let model = self?.mapForPresentable(cat) {
                        self?.categories.value[index] = model
                    }
                })
                
                deletions.forEach({ (index) in
                    self?.categories.value.remove(at: index)
                })
            case .error(_):
                break
            }
        })
        
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    func deleteCategory(index: Int) {
        let categoryVM = categories.value[index]
        if let id = categoryVM.id, let category = RealmDatabaseManager.shared.realm.object(ofType: CategoryModel.self, forPrimaryKey: id) {
            RealmDatabaseManager.shared.delete(category)
        }
    }
    
}

extension CategoriesViewModel {
    
    fileprivate func mapForPresentable(_ categoryEntity: CategoryModel) -> CategoryItemViewModel {
        let id = categoryEntity.id
        let name = categoryEntity.name
        let desc = categoryEntity.desc
        let coverPhoto = categoryEntity.coverPhoto
        
        let categoryViewModel =  CategoryItemViewModel(id: id, name: name, desc: desc, coverPhoto: coverPhoto)
        return categoryViewModel
    }
    
    func addButtonDidSelected() {
        print("Add Button Selected")
        let object = CategoryModel()
        object.name = "name: \(object.id)"
        object.desc = "desc: \(object.id)"
        RealmDatabaseManager.shared.create(object)
    }
}
