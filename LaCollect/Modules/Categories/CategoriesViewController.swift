//
//  CategoriesViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 26.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxCocoa

class CategoriesViewController: TableParentViewController {

    @IBOutlet weak var createButton: LCButton!
    
    var categoriesViewModel: CategoriesViewModel = CategoriesViewModel()
    
    override func viewDidLoad() {
        title = "CATEGORIES"
        super.viewDidLoad()
        
        addRightMenuButton(title: "", image: "ic_lock") {
            RealmWebService.logout()
            if let appdelegate =  UIApplication.shared.delegate as? AppDelegate {
                appdelegate.openLogin()
            }
        }
        
        addRefreshControl {
            self.categoriesViewModel = CategoriesViewModel()
            self.refreshControl.endRefreshing()
        }
        
        createButton.tap(rxBag: disposeBag) { [weak self] in
            self?.navigate(.modifications(.addCategory, id: nil))
        }
        
        categoriesViewModel.categories.asObservable()
            .bind(to: tableView.rx.items(cellIdentifier: CategoryTableViewCell.toString(),
                                         cellType: CategoryTableViewCell.self))
            { index, item, cell in
            cell.setupCell(self.categoriesViewModel.categories.value[index])
            cell.index = index
            cell.delegate = self
        }.disposed(by: disposeBag)
        
    }
    
}

extension CategoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let id = categoriesViewModel.categories.value[indexPath.row].id {
            navigate(.items(id))
        }
    }
}

extension CategoriesViewController: UpdateFromCellDelegate {
    func editDidSelected(_ index: Int) {
        self.navigate(.modifications(.editCategory, id: self.categoriesViewModel.categories.value[index].id))
    }
    
    func deleteDidSelected(_ index: Int) {
        self.categoriesViewModel.deleteCategory(index: index)
    }
}
