//
//  CategoryTableViewCell.swift
//  LaCollect
//
//  Created by Lellis Ginna on 28.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

protocol UpdateFromCellDelegate: class {
    func editDidSelected(_ index: Int)
    func deleteDidSelected(_ index: Int)
}

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var nameLabel: LCLabel!
    @IBOutlet weak var descLabel: LCLabel!
    
    weak var delegate: UpdateFromCellDelegate?
    var index: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(_ model: CategoryViewPresentable) {
        nameLabel.text = model.name
        descLabel.text = model.desc
        photoImageView.image = nil
        
        if let photoData = model.coverPhoto {
            photoImageView.image = UIImage(data: photoData)
        }
    }
    @IBAction func deleteButtonDidSelected(_ sender: Any) {
        delegate?.deleteDidSelected(index)
    }
    @IBAction func editButtonDidSelected(_ sender: Any) {
        delegate?.editDidSelected(index)
    }
}
