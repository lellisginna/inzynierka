//
//  ModificationsViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 16.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class ModificationsViewController: ParentViewController {
    
    @IBOutlet weak var saveButton: LCButton!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var galleryButton: LCButton!
    @IBOutlet weak var cameraButton: LCButton!
    
    var modificationsViewModel: ModificationsViewModel!

    override func viewDidLoad() {
        title = modificationsViewModel.screenType.screenTitle()
        super.viewDidLoad()
        
        configureDataView()
        addBackButton()
        configureButtons()
        configureTextViews()
    }
    
    func configureDataView() {
        if let data = modificationsViewModel.data {
            nameTextField.text = data.name
            descriptionTextView.text = data.desc
            pictureView.image = data.image ?? UIImage(named: "ic_image_placeholder")
        }
    }
    
    func configureButtons() {
        let mediaPicker: LCMediaPicker = LCMediaPicker()
        cameraButton.tap(rxBag: disposeBag) { [weak self] in
            guard let me = self else { return }
            mediaPicker.getImage = { image in
                me.modificationsViewModel.data.image = image
                me.pictureView.image = image
            }
            mediaPicker.show(from: me)
        }
        
        galleryButton.tap(rxBag: disposeBag) { [weak self] in
            guard let me = self else { return }
            mediaPicker.getImage = { image in
                me.modificationsViewModel.data.image = image
                me.pictureView.image = image
            }
            mediaPicker.show(source: .photoLibrary, from: me)
        }
        
        saveButton.tap(rxBag: disposeBag) { [weak self] in
            self?.modificationsViewModel.saveData()
            self?.pop()
            
        }
    }
    
    func configureTextViews() {
        nameTextField.rx.text.map { $0 ?? "" }.bind(to: modificationsViewModel.nameText).disposed(by: disposeBag)
        descriptionTextView.rx.text.map { $0 ?? "" }.bind(to: modificationsViewModel.descText).disposed(by: disposeBag)
    }
}
