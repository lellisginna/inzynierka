//
//  ModificationsViewModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 16.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxSwift
import RealmSwift

protocol ModifcationsViewPresentable {
    var name: String? { set get }
    var desc: String? { set get }
    var image: UIImage? { set get }
}

struct ModificationModelData: ModifcationsViewPresentable {
    var name: String?
    var desc: String?
    var image: UIImage?
    
    init(name: String? = nil, desc: String? = nil, image: UIImage? = nil) {
        self.name = name
        self.desc = desc
        self.image = image
    }
}

class ModificationsViewModel {
    
    fileprivate var id: String?
    
    var screenType: ModificationType = .addCategory
    var data: ModifcationsViewPresentable!
    
    var nameText = Variable<String>("")
    var descText = Variable<String>("")
    
    var category: CategoryModel?
    
    init(screenType: ModificationType, id: String? = nil) {
        self.screenType = screenType
        self.data = ModificationModelData()
        self.id = id
        
        if let id = id, let category: CategoryModel = RealmDatabaseManager.shared.realm.objects(CategoryModel.self).filter("id == %@", id).first {
            
            self.category = category
            
            if screenType == .editCategory {
                self.data.name = category.name
                self.data.desc = category.desc
                
                if let imageData = category.coverPhoto {
                    self.data.image = UIImage(data: imageData)
                }
                
                self.nameText.value = category.name ?? ""
                self.descText.value = category.desc ?? ""
            }
        }
    }
    
    func saveData() {
        switch screenType {
        case .addCategory, .addItem:
            addData()
        case .editCategory, .editItem:
            updateData()
        }
    }
    
    fileprivate func addData() {
        if screenType == .addCategory {
            let object = CategoryModel()
            object.name = nameText.value
            object.desc = descText.value
            object.coverPhoto = data.image?.toData()
            RealmDatabaseManager.shared.create(object)
        } else {
            if let category = category {
                let object = SingleItemModel()
                object.name = nameText.value
                object.desc = descText.value
                object.photo = data.image?.toData()
                
                do {
                    try RealmDatabaseManager.shared.realm.write {
                        category.items.append(object)
                    }
                } catch {
                    print("Error during save item")
                }
                RealmDatabaseManager.shared.create(category)
            }
        }
    }
    
    fileprivate func updateData() {
        if screenType == .editCategory {
            if let object = RealmDatabaseManager.shared.realm.object(ofType: CategoryModel.self, forPrimaryKey: id) {
                var params: [String: Any] = [:]
                params["name"] = nameText.value
                params["desc"] = descText.value
                params["coverPhoto"] = data.image?.toData()
                params["updatedAt"] = Date()
                RealmDatabaseManager.shared.update(object, with: params)
            }
        } else {
            if let object = RealmDatabaseManager.shared.realm.object(ofType: SingleItemModel.self, forPrimaryKey: id) {
                var params: [String: Any] = [:]
                params["name"] = nameText.value
                params["desc"] = descText.value
                params["photo"] = data.image?.toData()
                params["updatedAt"] = Date()
                RealmDatabaseManager.shared.update(object, with: params)
            }
        }
    }
}

