//
//  ItemsViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 27.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxSwift
import RxDataSources

class ItemsViewController: TableParentViewController {
    
    @IBOutlet weak var addItemButton: LCButton!
    
    var itemsViewModel: ItemViewModel!

    override func viewDidLoad() {
        title = "ITEMS"
        
        super.viewDidLoad()
        
        addBackButton()
        
        addItemButton.tap(rxBag: disposeBag) { [weak self] in
            self?.navigate(.modifications(.addItem, id: self?.itemsViewModel.category?.id))
        }
        
        itemsViewModel.items.asObservable().bind(to: tableView.rx.items(cellIdentifier: ItemTableViewCell.toString(), cellType: ItemTableViewCell.self)) { index, item, cell in
            cell.setupCell(self.itemsViewModel.items.value[index])
            cell.index = index
            cell.delegate = self
            }.disposed(by: disposeBag)
    }
}

extension ItemsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let activityItems = itemsViewModel.shareItemInformations(index: indexPath.row)
        share(activityItems: activityItems)
    }
}

extension ItemsViewController: UpdateFromCellDelegate {
    func editDidSelected(_ index: Int) {
    }
    
    func deleteDidSelected(_ index: Int) {
        self.itemsViewModel.deleteItem(index: index)
    }
}
