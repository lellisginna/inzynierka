//
//  ItemsViewModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 27.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

protocol ItemViewPresentable {
    var id: String? { get }
    var name: String? { get }
    var desc: String? { get }
    var photo: Data? { get }
}

struct SingleItemViewModel: ItemViewPresentable {
    var id: String?
    var name: String?
    var desc: String?
    var photo: Data?
    
    init(id: String?, name: String?, desc: String?, photo: Data?) {
        self.id = id
        self.name = name
        self.desc = desc
        self.photo = photo
    }
}

class ItemViewModel {
    
    var items: Variable<[ItemViewPresentable]> = Variable([])
    var category: CategoryModel?
    var notificationToken: NotificationToken?
    
    init(id: String) {
        category = RealmDatabaseManager.shared.realm.object(ofType: CategoryModel.self, forPrimaryKey: id)
        
        notificationToken = category?.items._observe({ [weak self] (changes: RealmCollectionChange) in
            guard let me = self else { return }
            switch changes {
            case .initial:
                me.category?.items.forEach({ (item) in
                    me.items.value.append(me.mapForPresentable(item))
                })
                
            case .update(_, let deletions, let insertions, let modifications):
                guard let itemsDB = me.category?.items else { return }
                insertions.forEach({ (index) in
                    let itemEntity = itemsDB[index]
                    let itemViewModel = me.mapForPresentable(itemEntity)
                    me.items.value.append(itemViewModel)
                })
                
                modifications.forEach({ (index) in
                    let itemEntity = itemsDB[index]
                    
                    if itemEntity.isInvalidated {
                        me.items.value.remove(at: index)
                    }
                    
                    if let cat = RealmDatabaseManager.shared.realm.object(ofType: SingleItemModel.self, forPrimaryKey: itemEntity.id), let model = self?.mapForPresentable(cat) {
                        me.items.value[index] = model
                    }
                })
                
                deletions.forEach({ (index) in
                    guard me.items.value.indices.contains(index) else { return }
                    me.items.value.remove(at: index)
                })
                
            default:
                break
            }
        })
    }
    
    deinit {
        notificationToken?.invalidate()
    }
    
    func deleteItem(index: Int) {
        let itemVM = category?.items[index]
        if let id = itemVM?.id, let item = RealmDatabaseManager.shared.realm.object(ofType: SingleItemModel.self, forPrimaryKey: id) {
            RealmDatabaseManager.shared.delete(item)
        }
    }
    
    func shareItemInformations(index: Int) -> [Any] {
        let itemVM = category?.items[index]
        if let id = itemVM?.id, let item = RealmDatabaseManager.shared.realm.object(ofType: SingleItemModel.self, forPrimaryKey: id) {
            var activitItems: [Any] = []
            var text = ""
            if let name = item.name {
                text = name
            }
            if let desc = item.desc {
                text = text.isEmpty ? desc : "\(text)\n\n\(desc)"
                
            }
            if let imageData = item.photo, let image = UIImage(data: imageData) {
                activitItems.append(image)
            }
            activitItems.append(text)
            return activitItems
        }
        return []
    }
}

extension ItemViewModel {
    fileprivate func mapForPresentable(_ itemEntity: SingleItemModel) -> SingleItemViewModel {
        let id = itemEntity.id
        let name = itemEntity.name
        let desc = itemEntity.desc
        let photo = itemEntity.photo
        
        let itemViewModel =  SingleItemViewModel(id: id, name: name, desc: desc, photo: photo)
        return itemViewModel
    }
}
