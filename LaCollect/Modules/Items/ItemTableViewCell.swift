//
//  ItemTableViewCell.swift
//  LaCollect
//
//  Created by Lellis Ginna on 26.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {

    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var nameLabel: LCLabel!
    @IBOutlet weak var pictureView: UIImageView!
    @IBOutlet weak var descLabel: LCLabel!
    
    weak var delegate: UpdateFromCellDelegate?
    var index: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    
    func setupCell(_ model: ItemViewPresentable) {
        nameLabel.text = model.name
        descLabel.text = model.desc
        pictureView.image = UIImage(named: "ic_image_placeholder")
        
        if let photoData = model.photo {
            pictureView.image = UIImage(data: photoData)
        }
    }
    
    @IBAction func deleteButtonDidSelected(_ sender: Any) {
        delegate?.deleteDidSelected(index)
    }
}
