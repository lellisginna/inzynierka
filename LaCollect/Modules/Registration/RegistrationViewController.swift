//
//  RegistrationViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 06.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RegistrationViewController: ParentViewController {

    @IBOutlet weak var registerButton: LCButton!
    @IBOutlet weak var usernameTextField: LCTextField!
    @IBOutlet weak var passwordTextField: LCTextField!
    @IBOutlet weak var rePasswordTextField: LCTextField!
    
    var registrationViewModel: RegistrationViewModel = RegistrationViewModel()
    
    override func viewDidLoad() {
        title = "REGISTRATION"
        super.viewDidLoad()
        
        addBackButton()
        
        usernameTextField.rx.text.map { $0 ?? "" }.bind(to: registrationViewModel.usernameText).disposed(by: disposeBag)
        passwordTextField.rx.text.map { $0 ?? "" }.bind(to: registrationViewModel.passwordText).disposed(by: disposeBag)
        rePasswordTextField.rx.text.map { $0 ?? "" }.bind(to: registrationViewModel.rePasswordText).disposed(by: disposeBag)
        
        registrationViewModel.isValid.bind(to: registerButton.rx.isEnabled).disposed(by: disposeBag)
        
        registrationViewModel.isValidUsername.subscribe(onNext: { (isValid) in
            self.usernameTextField.textColor = isValid ? UIColor.black : UIColor.red
        }).disposed(by: disposeBag)
        
        registrationViewModel.isValidPassword.subscribe(onNext: { (isValid) in
            self.passwordTextField.textColor = isValid ? UIColor.black : UIColor.red
        }).disposed(by: disposeBag)
        
        registrationViewModel.isValidRePassword.subscribe(onNext: { (isValid) in
            self.rePasswordTextField.textColor = isValid ? UIColor.black : UIColor.red
        }).disposed(by: disposeBag)
        
        registerButton.tap(rxBag: disposeBag) { [weak self] in
            RealmWebService.register(username: self?.registrationViewModel.usernameText.value, password: self?.registrationViewModel.passwordText.value) {
                self?.navigate(.categories)
            }
        }
        
    }
}
