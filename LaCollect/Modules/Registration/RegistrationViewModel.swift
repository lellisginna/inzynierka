//
//  RegistrationViewModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 15.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation
import RxSwift

struct RegistrationViewModel {
    
    var usernameText = Variable<String>("")
    var passwordText = Variable<String>("")
    var rePasswordText = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(usernameText.asObservable(), passwordText.asObservable(), rePasswordText.asObservable()) { email, password, rePassword in
            self.usernameText.value.count >= 5 && self.passwordText.value.count >= 6 && self.rePasswordText.value == self.passwordText.value
        }
    }
    
    var isValidUsername: Observable<Bool> {
        return Observable.combineLatest(usernameText.asObservable(), usernameText.asObservable()) { username, pass in
            self.usernameText.value.count >= 5
        }
    }
    
    var isValidPassword: Observable<Bool> {
        return Observable.combineLatest(passwordText.asObservable(), passwordText.asObservable()) { username, pass in
            self.passwordText.value.count >= 6
        }
    }
    
    var isValidRePassword: Observable<Bool> {
        return Observable.combineLatest(rePasswordText.asObservable(), rePasswordText.asObservable()) { username, pass in
            self.rePasswordText.value.count >= 6
        }
    }
}
