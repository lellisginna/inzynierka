//
//  LoginViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 24.01.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RealmSwift

class LoginViewController: ParentViewController {

    @IBOutlet weak var loginTextField: LCTextField!
    @IBOutlet weak var passwordTextField: LCTextField!
    @IBOutlet weak var loginButton: LCButton!
    @IBOutlet weak var registerButton: LCButton!
    
    var loginViewModel: LoginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginTextField.rx.text.map { $0 ?? "" }.bind(to: loginViewModel.usernameText).disposed(by: disposeBag)
        passwordTextField.rx.text.map { $0 ?? "" }.bind(to: loginViewModel.passwordText).disposed(by: disposeBag)
        
        loginViewModel.isValid.bind(to: loginButton.rx.isEnabled).disposed(by: disposeBag)
        
        loginViewModel.isValidUsername.subscribe(onNext: { (isValid) in
            self.loginTextField.textColor = isValid ? UIColor.black : UIColor.red
        }).disposed(by: disposeBag)
        
        loginViewModel.isValidPassword.subscribe(onNext: { (isValid) in
            self.passwordTextField.textColor = isValid ? UIColor.black : UIColor.red
        }).disposed(by: disposeBag)
        
        loginButton.tap(rxBag: disposeBag) { [weak self] in
            RealmWebService.login(username: self?.loginViewModel.usernameText.value, password: self?.loginViewModel.passwordText.value) {
                self?.navigate(.categories)
            }
        }
        
        registerButton.tap(rxBag: disposeBag) { [weak self] in
            self?.navigate(.register)
        }
    }
}
