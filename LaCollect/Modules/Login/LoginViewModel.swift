//
//  LoginViewModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 02.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation
import RxSwift

struct LoginViewModel {
    
    private let disposeBag = DisposeBag()
    
    var usernameText = Variable<String>("")
    var passwordText = Variable<String>("")
    
    var isValid: Observable<Bool> {
        return Observable.combineLatest(usernameText.asObservable(), passwordText.asObservable()) { email, password in
            self.usernameText.value.count >= 5 && self.passwordText.value.count >= 6
        }
    }
    
    var isValidUsername: Observable<Bool> {
        return usernameText.asObservable().flatMap({ (text) -> Observable<Bool> in
            let valid = text.count >= 5
            return Observable.just(valid)
        })
    }
    
    var isValidPassword: Observable<Bool> {
        return passwordText.asObservable().flatMap({ (text) -> Observable<Bool> in
            let valid = text.count >= 6
            return Observable.just(valid)
        })
    }
}
