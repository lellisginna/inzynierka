//
//  LCAlerts.swift
//  LaCollect
//
//  Created by Lellis Ginna on 17.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation
import UIKit

final class LCAlerts: NSObject {
    
    public static func snackbar(_ message: String) {
        if let vc = UIApplication.topViewController() as? ParentViewController {
            vc.snackbar(message: message)
        }
    }
}
