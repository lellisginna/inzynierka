//
//  NetworkActivityIndicatorManager.swift
//  lets-meet
//
//  Created by Lellis Ginna on 08.01.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class NetworkActivityIndicatorManager: NSObject {

    private static var loadingCount = 0

    class var isLoading: Bool {
        return loadingCount > 0
    }

    class func networkOperationStarted() {
        if loadingCount == 0 {
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = true
            }
        }

        loadingCount += 1
    }

    class func networkOperationFinished() {
        if loadingCount > 0 {
            loadingCount -= 1
        }

        if loadingCount == 0 {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
}
