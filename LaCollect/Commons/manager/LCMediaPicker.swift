//
//  LCMediaPicker.swift
//  LaCollect
//
//  Created by Lellis Ginna on 24.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation
import UIKit

typealias ImagePickerAndNavigationDelegate = UIImagePickerControllerDelegate & UINavigationControllerDelegate

class LCMediaPicker: NSObject {
    
    fileprivate let imagePicker = UIImagePickerController()
    fileprivate var image: UIImage?
    
    var getImage: ((_ image: UIImage?) -> Void)?
    
    func show(source: UIImagePickerControllerSourceType = .camera, from vc: UIViewController) {
        imagePicker.delegate = self
        imagePicker.sourceType = source
        vc.navigationController?.present(imagePicker, animated: true, completion: nil)
    }
}

extension LCMediaPicker: ImagePickerAndNavigationDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        getImage?(image?.compressTo(1))
        imagePicker.dismiss(animated: true, completion: nil)
    }
}
