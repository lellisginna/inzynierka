//
//  RealmDataManager.swift
//  LaCollect
//
//  Created by Lellis Ginna on 03.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import Realm
import RealmSwift

class RealmWebService: NSObject {

    public static var logged: Bool {
        if let _ = SyncUser.current {
            return true
        }
        return false
    }
    
    public static func login(username: String?, password: String?, success: @escaping () -> Void) {
        guard let username = username, let password = password else { return }
        let creds = SyncCredentials.usernamePassword(username: username, password: password)
        login(credentials: creds) {
            success()
        }
    }
    
     public static func register(username: String?, password: String?, success: @escaping () -> Void) {
        guard let username = username, let password = password else { return }
        let creds = SyncCredentials.usernamePassword(username: username, password: password, register: true)
        login(credentials: creds) {
            success()
        }
    }
    
    public static func logout() {
        SyncUser.current?.logOut()
    }
    
    fileprivate static func login(credentials: SyncCredentials, success: @escaping () -> Void) {
        if logged {
            success()
        } else {
            NetworkActivityIndicatorManager.networkOperationStarted()
            SyncUser.logIn(with: credentials, server: RREALM_AUTH_URL, onCompletion: { (user, error) in
                NetworkActivityIndicatorManager.networkOperationFinished()
                if let _ = user {
                    RealmDatabaseManager.shared.openRealm()
                    success()
                } else if let error = error {
                    LCAlerts.snackbar(error.localizedDescription)
                    print("LOGIN ERROR: \(error.localizedDescription)")
                }
            })
        }
    }
}


