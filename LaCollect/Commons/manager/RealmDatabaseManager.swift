//
//  RealmDatabaseManager.swift
//  LaCollect
//
//  Created by Lellis Ginna on 28.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import RealmSwift

class RealmDatabaseManager {
    
    init() {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: REALM_URL)
        Realm.Configuration.defaultConfiguration = Realm.Configuration(syncConfiguration: syncConfig, objectTypes:[CategoryModel.self, SingleItemModel.self])
        realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig, objectTypes:[CategoryModel.self, SingleItemModel.self]))
    }
    
    static let shared = RealmDatabaseManager()
    
    var realm: Realm!
    var localRealm: Realm = try! Realm()
    
    func openRealm() {
        let syncConfig = SyncConfiguration(user: SyncUser.current!, realmURL: REALM_URL)
        Realm.Configuration.defaultConfiguration = Realm.Configuration(syncConfiguration: syncConfig, objectTypes:[CategoryModel.self, SingleItemModel.self])
        realm = try! Realm(configuration: Realm.Configuration(syncConfiguration: syncConfig, objectTypes:[CategoryModel.self, SingleItemModel.self]))
    }
    
    func create<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.add(object, update: true)
            }
        }catch {
            print(error)
        }
    }
    
    func update<T: Object>(_ object: T, with dictionary: [String : Any?]) {
        do {
            try realm.write {
                for (key, value) in dictionary {
                    object.setValue(value, forKey: key)
                }
            }
        } catch {
            print(error)
        }
    }
    
    func delete<T: Object>(_ object: T) {
        do {
            try realm.write {
                realm.delete(object)
            }
        } catch {
            print(error)
        }
    }
    
    func deleteLocalAll() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            print(error)
        }
    }
}
