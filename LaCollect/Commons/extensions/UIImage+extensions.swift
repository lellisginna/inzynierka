//
//  UIImage+extensions.swift
//  LaCollect
//
//  Created by Lellis Ginna on 24.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

extension UIImage {
    func toData() -> Data? {
        return UIImagePNGRepresentation(self)
    }
    
    func compressTo(_ expectedSizeInMb: Int) -> UIImage? {
        let sizeInBytes = expectedSizeInMb * 1024 * 1024
        var needCompress: Bool = true
        var imgData: Data?
        var compressingValue: CGFloat = 1.0
        while needCompress && compressingValue > 0.0 {
            print("Kompresja \(compressingValue)")
            if let data: Data = UIImageJPEGRepresentation(self, compressingValue) {
                if data.count < sizeInBytes {
                    needCompress = false
                    imgData = data
                } else {
                    compressingValue -= 0.1
                }
            }
        }
        
        if let data = imgData {
            if data.count < sizeInBytes {
                return UIImage(data: data, scale: compressingValue)
            }
        }
        return nil
    }
}
