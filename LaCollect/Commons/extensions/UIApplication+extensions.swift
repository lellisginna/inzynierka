//
//  UIApplication+extensions.swift
//  LaCollect
//
//  Created by Lellis Ginna on 17.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

extension UIApplication {
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.topViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController, presented as? UIAlertController == nil {
            return topViewController(presented)
        }
        
        return viewController
    }
}
