//
//  UIColor+extensions.swift
//  LaCollect
//
//  Created by Lellis Ginna on 04.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import UIColor_Hex

extension UIColor {
    
    static let darkBrown: UIColor = UIColor(css: "#5A2729")
    static let regularBrown: UIColor = UIColor(css: "#763931")
    static let greyBrown: UIColor = UIColor(css: "#91554D")
    static let lightBrown: UIColor = UIColor(css: "#AB9584")
    
}
