//
//  UIButton+Extensions.swift
//  LaCollect
//
//  Created by Lellis Ginna on 14.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

extension UIButton {
    public func setTitle(_ title: String?) {
        setTitle(title, for: .normal)
        setTitle(title, for: .highlighted)
        setTitle(title, for: .selected)
    }
    
    public func setAttributedTitle(_ title: NSAttributedString?) {
        setAttributedTitle(title, for: .normal)
        setAttributedTitle(title, for: .highlighted)
        setAttributedTitle(title, for: .selected)
    }
    
    public func setImage(_ image: UIImage?) {
        setImage(image, for: .normal)
        setImage(image, for: .highlighted)
        setImage(image, for: .selected)
    }
    
    public func setTitleWithoutAnimation(title: String?) {
        UIView.setAnimationsEnabled(false)
        setTitle(title)
        layoutIfNeeded()
        UIView.setAnimationsEnabled(true)
    }
    
    public func tap(rxBag: DisposeBag?, closure: @escaping () -> Void) {
        if let rxBag = rxBag {
            rx.tap.asDriver().throttle(3).drive(onNext: { _ in
                closure()
            }).disposed(by: rxBag)
        }
    }
}
