//
//  UIViewController+extension.swift
//  LaCollect
//
//  Created by Lellis Ginna on 06.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

extension UIViewController {
    static func toString() -> String {
        return String(describing: self)
    }
    
    func navigate(_ navigation: LCNavigation) {
        navigate(navigation as Navigation)
    }
    
    func setStatusBarBackgroundColor(color: UIColor) {
        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
        if statusBar.responds(to: #selector(setter: UIView.backgroundColor)) {
            statusBar.backgroundColor = color
            statusBar.tintColor = UIColor.white
        }
        setNeedsStatusBarAppearanceUpdate()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    var previousViewController: UIViewController? {
        if let controllersOnNavStack = self.navigationController?.viewControllers {
            let n = controllersOnNavStack.count
            // if self is still on Navigation stack
            if controllersOnNavStack.last === self, n > 1 {
                return controllersOnNavStack[n - 2]
            } else if n > 0 {
                return controllersOnNavStack[n - 1]
            }
        }
        return nil
    }
    
}
