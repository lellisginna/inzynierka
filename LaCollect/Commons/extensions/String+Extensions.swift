//
//  String+Extensions.swift
//  LaCollect
//
//  Created by Lellis Ginna on 03.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
    func localizedFromStringDictWith(value: CVarArg) -> String {
        let format = NSLocalizedString(self, comment: "")
        return String.localizedStringWithFormat(format, value)
    }
    
    func localizedWith(value: Int?) -> String {
        let stringBefore = localized
        let stringFromat = String.localizedStringWithFormat(stringBefore, value ?? 0)
        return stringFromat
    }
    
    func localizedWith(value: String?) -> String {
        let stringBefore = localized
        let stringFromat = String.localizedStringWithFormat(stringBefore, value ?? "")
        return stringFromat
    }
    
    func localizedWith(args: [CVarArg]) -> String {
        let localizedFormat = localized
        var stringArgs = [CVarArg]()
        for arg in args {
            stringArgs.append("\(arg)")
        }
        
        return withVaList(args) {
            NSString(format: localizedFormat, arguments: $0)
            } as String
    }
    
    func localizedWith(args: CVarArg...) -> String {
        return localizedWith(args: args)
    }
    
    func isEmpty() -> Bool {
        let trimmedString = trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString.isEmpty
    }
    
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    func isValidPasswordRules() -> Bool {
        return textLenghtMin(6) && isContainsNumber()
    }
    
    func isContainsNumber() -> Bool {
        let numberRegEx = ".*[0-9]+.*"
        let testCase = NSPredicate(format: "SELF MATCHES %@", numberRegEx)
        let containsNumber = testCase.evaluate(with: self)
        return containsNumber
    }
    
    func textLenghtMin(_ min: Int) -> Bool {
        if count >= min {
            return true
        }
        return false
    }
    
    func textLenghtMax(_ max: Int) -> Bool {
        if count > max {
            return false
        }
        return true
    }
}

