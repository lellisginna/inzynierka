//
//  UIView+extensions.swift
//  LaCollect
//
//  Created by Lellis Ginna on 04.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

extension UIView {
    static func toString() -> String {
        return String(describing: self)
    }
}
