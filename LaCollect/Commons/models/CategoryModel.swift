//
//  CategoryModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 27.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

import RealmSwift

class CategoryModel: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String?
    @objc dynamic var desc: String?
    @objc dynamic var coverPhoto: Data?
    
    @objc dynamic var createdAt: Date? = Date()
    @objc dynamic var updatedAt: Date?
    @objc dynamic var deletedAt: Date?
    
    var items: List<SingleItemModel> = List<SingleItemModel>()
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
     func setup(id: String, name: String?, desc: String?, coverPhoto: Data) {
        self.id = id
        self.name = name
        self.desc = desc
        self.coverPhoto = coverPhoto
    }
}

