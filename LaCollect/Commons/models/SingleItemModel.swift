//
//  SingleItemModel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 28.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation

import UIKit
import RealmSwift

class SingleItemModel: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String?
    @objc dynamic var desc: String?
    @objc dynamic var photo: Data?
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}

