//
//  LCHelper.swift
//  LaCollect
//
//  Created by Lellis Ginna on 14.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import Device

var LCH = LCHelper.shared

class LCHelper: NSObject {
    
    static let shared: LCHelper = LCHelper()
    
    func fontSizeForDevice(withSize size: CGFloat) -> CGFloat {
        switch Device.size() {
        case .screen3_5Inch:
            return size * 0.7
        case .screen4Inch:
            return size
        case .screen4_7Inch:
            return size * 1.2
        case .screen5_5Inch:
            return size * 1.4
        case .screen5_8Inch:
            return size * 1.3
        case .screen7_9Inch:
            return size * 1.6
        case .screen9_7Inch:
            return size * 1.8
        case .screen12_9Inch:
            return size * 2.0
        case .unknownSize:
            return size
        default:
            return size
        }
    }

}
