//
//  LCConstants.swift
//  LaCollect
//
//  Created by Lellis Ginna on 03.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation
import UIKit

let REALM_INSTANCE_ADDRESS:String = "lacollect.us1.cloud.realm.io"
let RREALM_AUTH_URL: URL = URL(string: "https://\(REALM_INSTANCE_ADDRESS)")!
let REALM_URL: URL = URL(string: "realms://\(REALM_INSTANCE_ADDRESS)/~/UserData")!

let alertViewHeight: CGFloat = 80
let topViewTag = 123_456
let rightMenuButtonTag = 234_500
let rightMenuSecondButtonTag = 234_502
let leftMenuButtonTag = 234_501


// ENUMS

enum ModificationType {
    case addCategory
    case editCategory
    case addItem
    case editItem
    
    func screenTitle() -> String {
        switch self {
        case .addCategory:
            return "Add Category"
        case .editCategory:
            return "Edit Category"
        case .addItem:
            return "Add Item"
        case .editItem:
            return "Edit Item"
        }
    }
    
}
