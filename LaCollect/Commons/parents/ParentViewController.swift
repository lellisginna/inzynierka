//
//  ParentViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 24.01.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import TTGSnackbar
import RxSwift

class ParentViewController: UIViewController {
    
    fileprivate var isAlertVisible = false
    var titleLabelWidth: CGFloat = 138
    var titleLabel: LCLabel?
    
    let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        addCustomNavigationBar()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func push(_ vc: UIViewController) {
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func pop(_ toRoot: Bool = false) {
        if toRoot {
            navigationController?.popToRootViewController(animated: true)
        } else {
            navigationController?.popViewController(animated: true)
        }
    }
    
    func snackbar(message: String, duration: TTGSnackbarDuration = .short) {
        let snackbar = TTGSnackbar(message: message, duration: duration)
        snackbar.show()
    }
    
    func share(activityItems: [Any]) {
        let activityViewController = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    //==============================================================================================================================================
    // CUSTOM NAVIGATION CONTROLLER
    //==============================================================================================================================================
    
    fileprivate func addCustomNavigationBar() {
        if let title = title, title.count > 0, let topView = addGradientBackground() {
            let label = LCLabel()
            label.textColor = UIColor.white
            label.textAlignment = .center
            label.baselineAdjustment = .alignCenters
            label.DeviceFontSize = 15
            topView.addSubview(label)
            label.translatesAutoresizingMaskIntoConstraints = false
            label.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
            label.centerXAnchor.constraint(equalTo: topView.centerXAnchor).isActive = true
            label.heightAnchor.constraint(equalToConstant: 40).isActive = true
            label.widthAnchor.constraint(equalTo: topView.widthAnchor, multiplier: 1, constant: -150).isActive = true
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.8
            titleLabel = label
            titleLabel?.text = title.uppercased()
        }
    }
    
    fileprivate func addGradientBackground() -> UIView? {
        let topView = GradientView()
        topView.tag = topViewTag
        view.addSubview(topView)
        
        topView.translatesAutoresizingMaskIntoConstraints = false
        topView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        topView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        topView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        topView.bottomAnchor.constraint(equalTo: topLayoutGuide.bottomAnchor, constant: 40).isActive = true
        
        topView.backgroundColor = UIColor.lightBrown
        topView.topColor = UIColor.darkBrown
        topView.bottomColor = UIColor.greyBrown
        return topView
    }
    
    func addBackButton() {
        addLeftMenuButton(title: "", image: "ic_back", closure: { [weak self] in
            self?.actionOnSelectCloseButton()
        })
    }
    
    func actionOnSelectCloseButton() {
        pop()
    }
    
    func addLeftMenuButton(title: String, image: String, closure: @escaping () -> Void) {
        if let topView = view.viewWithTag(topViewTag) {
            let button = UIButton()
            button.tag = leftMenuButtonTag
            button.contentHorizontalAlignment = .left
            
            if image.count > 0 {
                button.setImage(UIImage(named: image))
                button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
            }
            
            button.setTitle(title, for: .normal)
            button.setTitleColor(UIColor.white, for: .normal)
            button.tintColor = UIColor.white
            button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
            
            button.tap(rxBag: disposeBag, closure: {
                closure()
            })
            
            topView.addSubview(button)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
            button.leadingAnchor.constraint(equalTo: topView.leadingAnchor, constant: 0).isActive = true
            button.heightAnchor.constraint(equalToConstant: 40).isActive = true
            button.widthAnchor.constraint(equalToConstant: 80).isActive = true
        }
    }
    
    func addRightMenuButton(title: String, image: String, closure: @escaping () -> Void) {
        if let topView = view.viewWithTag(topViewTag) {
            if let rightMenuButton = topView.viewWithTag(rightMenuButtonTag) {
                rightMenuButton.isHidden = false
            } else {
                let button = UIButton()
                button.tag = rightMenuButtonTag
                button.contentHorizontalAlignment = .right
                
                if image.count > 0 {
                    button.setImage(UIImage(named: image))
                    button.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
                }
                
                button.setTitle(title, for: .normal)
                button.setTitleColor(UIColor.white, for: .normal)
                button.tintColor = UIColor.white
                button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 18)
                
                button.tap(rxBag: disposeBag, closure: {
                    closure()
                })
                
                topView.addSubview(button)
                button.translatesAutoresizingMaskIntoConstraints = false
                button.bottomAnchor.constraint(equalTo: topView.bottomAnchor, constant: 0).isActive = true
                button.trailingAnchor.constraint(equalTo: topView.trailingAnchor, constant: 0).isActive = true
                button.heightAnchor.constraint(equalToConstant: 40).isActive = true
                button.widthAnchor.constraint(equalToConstant: 80).isActive = true
            }
        }
    }
}
