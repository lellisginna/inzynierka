//
//  TableParentViewController.swift
//  LaCollect
//
//  Created by Lellis Ginna on 24.01.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import ActionKit

class TableParentViewController: ParentViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var topConstraintCopy: NSLayoutConstraint?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        automaticallyAdjustsScrollViewInsets = false
        
        if tableView != nil {
            tableView.tableFooterView = UIView()
            tableView.backgroundColor = UIColor.white
        }
    }
    
    func addRefreshControl(closure: @escaping () -> Void) {
        refreshControl.addControlEvent(.valueChanged, {
            closure()
        })
        tableView.addSubview(refreshControl)
    }
}
