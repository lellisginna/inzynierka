//
//  Dependencies.swift
//  LaCollect
//
//  Created by Lellis Ginna on 26.07.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import Foundation

protocol HasRealmWebService {
    var realmWebService: RealmWebService  { get }
}

protocol HasRealmDatabase {
    var realmDatabase: RealmDatabaseManager { get }
}

typealias AllDependencies = HasRealmWebService & HasRealmDatabase

class Dependencies: AllDependencies {
    let realmWebService: RealmWebService  = RealmWebService()
    let realmDatabase:RealmDatabaseManager = RealmDatabaseManager()
}
