//
//  LCRouter.swift
//  LaCollect
//
//  Created by Lellis Ginna on 26.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

enum LCNavigation: Navigation {
    case login
    case register
    case categories
    case modifications(ModificationType, id: String?)
    case items(String)
}

struct LCAppNavigation: AppNavigation {
    
    fileprivate func viewControllerFromStoryboard(_ storyboardName: String, identifier: String) -> UIViewController {
        return UIStoryboard(name: storyboardName, bundle: nil).instantiateViewController(withIdentifier: identifier)
    }
    
    func viewcontrollerForNavigation(navigation: Navigation) -> UIViewController {
        if let navigation = navigation as? LCNavigation {
            switch navigation {
            case .login:
                return viewControllerFromStoryboard("Main", identifier: LoginViewController.toString()) as! LoginViewController
            case .register:
                return viewControllerFromStoryboard("Registration", identifier: RegistrationViewController.toString())
            case .categories:
                return viewControllerFromStoryboard("Categories", identifier: CategoriesViewController.toString())
            case .modifications(let type, let id):
                let vc = viewControllerFromStoryboard("Moficications", identifier: ModificationsViewController.toString()) as! ModificationsViewController
                vc.modificationsViewModel = ModificationsViewModel(screenType: type, id: id)
                return vc
            case .items(let id):
                let vc = viewControllerFromStoryboard("Items", identifier: ItemsViewController.toString()) as! ItemsViewController
                vc.itemsViewModel = ItemViewModel(id: id)
                return vc
            }
        }
        return UIViewController()
    }
    
    func navigate(_ navigation: Navigation, from: UIViewController, to: UIViewController) {
        from.navigationController?.pushViewController(to, animated: true)
    }
}


