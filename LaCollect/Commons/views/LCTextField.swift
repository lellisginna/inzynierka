//
//  LCTextField.swift
//  LaCollect
//
//  Created by Lellis Ginna on 05.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class LCTextField: UITextField {
    
    let disposedBag = DisposeBag()
    var bottomBorder = RoundedView()
    @IBInspectable var borderColor: UIColor = UIColor.lightBrown
    @IBInspectable var selectedBorderColor: UIColor = UIColor.lightBrown
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setBottomBorder()
        self.rx.controlEvent([.editingDidBegin, .editingDidEnd])
        .asObservable()
            .subscribe { [weak self] (event) in
                if self?.bottomBorder.backgroundColor == UIColor.darkBrown {
                    self?.bottomBorder.backgroundColor = self?.borderColor
                } else {
                    self?.bottomBorder.backgroundColor = UIColor.darkBrown
                }
        }.disposed(by: disposedBag)
    }
    
    func setBottomBorder() {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        bottomBorder = RoundedView.init(frame: CGRect(x: 10, y: 0, width: 0, height: 0))
        bottomBorder.cornerRadius = 20
        bottomBorder.backgroundColor = borderColor
        bottomBorder.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(bottomBorder)
        
        bottomBorder.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        bottomBorder.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        bottomBorder.rightAnchor.constraint(equalTo: rightAnchor).isActive = true
        bottomBorder.heightAnchor.constraint(equalToConstant: 1).isActive = true // Set Border-Strength
    }
}
