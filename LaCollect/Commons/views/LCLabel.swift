//
//  LCLabel.swift
//  LaCollect
//
//  Created by Lellis Ginna on 14.06.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class LCLabel: UILabel {
    @IBInspectable var uppercased: Bool = false
    
    override func awakeFromNib() {
        if uppercased {
            super.text = text?.uppercased()
        }
        super.awakeFromNib()
    }
    
    public override var text: String? {
        didSet {
            if uppercased {
                super.text = text?.uppercased()
            } else {
                super.text = text
            }
        }
    }
    
    @IBInspectable var DeviceFontSize: CGFloat = 0 {
        didSet {
            overrideFontSize(fontSize: DeviceFontSize)
        }
    }
    
    func overrideFontSize(fontSize: CGFloat) {
        let currentFontName = font.fontName
        font = UIFont(name: currentFontName, size: LCH.fontSizeForDevice(withSize: fontSize))
        text = NSLocalizedString(text ?? "", comment: "")
    }
}
