//
//  GradientView.swift
//  LaCollect
//
//  Created by Lellis Ginna on 04.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class GradientView: RoundedView {
    
    @IBInspectable var topColor: UIColor = UIColor.white
    @IBInspectable var bottomColor: UIColor = UIColor.white
    
    override func draw(_ rect: CGRect) {
        updateColors()
        super.draw(rect)
    }
    
    func updateColors() {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.colors = [topColor.cgColor, bottomColor.cgColor]
        layer.insertSublayer(gradientLayer, at: 0)
    }
    
}
