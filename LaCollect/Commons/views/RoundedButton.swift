//
//  RoundedButton.swift
//  LaCollect
//
//  Created by Lellis Ginna on 04.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class RoundedButton: UIButton {
    
    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor = UIColor.white
    @IBInspectable var showShadow: Bool = false
    @IBInspectable var DeviceFontSize: CGFloat = 0 {
        didSet {
            overrideFontSize(fontSize: DeviceFontSize)
        }
    }
    
    var shadowLayer: UIView!
    
    func overrideFontSize(fontSize: CGFloat) {
        if let label = self.titleLabel, let font = label.font {
            let currentFontName = font.fontName
            label.font = UIFont(name: currentFontName, size: LCH.fontSizeForDevice(withSize: fontSize))
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layout()
    }
    
    func layout() {
        if cornerRadius == -1 {
            cornerRadius = frame.size.height / 2
        }
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = cornerRadius > 0
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        if shadowLayer != nil || !showShadow { return }
        shadowLayer = UIView(frame: frame)
        shadowLayer.backgroundColor = UIColor.clear
        shadowLayer.layer.shadowColor = UIColor.lightGray.cgColor
        shadowLayer.layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        shadowLayer.layer.shadowOffset = CGSize(width: -1.0, height: 1.0)
        shadowLayer.layer.shadowOpacity = 0.5
        shadowLayer.layer.shadowRadius = 2
        shadowLayer.layer.masksToBounds = true
        shadowLayer.clipsToBounds = false
        
        superview?.addSubview(shadowLayer)
        superview?.bringSubview(toFront: self)
    }

}
