//
//  RoundedImageView.swift
//  LaCollect
//
//  Created by Lellis Ginna on 04.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class RoundedImageView: UIImageView {

    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor = UIColor.white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        update()
    }
    
    func update() {
        if cornerRadius == -1 {
            cornerRadius = frame.size.width / 2
        }
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
    
}
