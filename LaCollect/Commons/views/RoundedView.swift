//
//  RoundedView.swift
//  LaCollect
//
//  Created by Lellis Ginna on 04.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var borderWidth: CGFloat = 0
    @IBInspectable var borderColor: UIColor = UIColor.white
    
    override func awakeFromNib() {
        super.awakeFromNib()
        updateView()
    }
    
    func updateView() {
        if cornerRadius == -1 {
            cornerRadius = frame.size.height / 2
        }
        clipsToBounds = true
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
    }
    
}
