//
//  LCButton.swift
//  LaCollect
//
//  Created by Lellis Ginna on 05.03.2018.
//  Copyright © 2018 Lellis Ginna. All rights reserved.
//

import UIKit

class LCButton: RoundedButton {
    
    override func awakeFromNib() {
        cornerRadius = 25
        super.awakeFromNib()
        titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
    }
}
